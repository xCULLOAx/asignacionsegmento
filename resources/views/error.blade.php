<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta content="text/html; charset="utf-8" http-equiv="content-type"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="keywords" name="PERÚ,  INEI ,  Perú, economy , INFORMATICA, INSTITUTO, ECONOMIA, Statistical Instituto Nacional de Estadística e Informática,estadisticas, National Institute of Statistics, Censos, EncuestasIndicadores, PBI, pbi, IPC, ipc, Índice de Precios, indice de Precios, price tax" />
        <meta content="description" name="INEI PERÚ El Instituto Nacional de Estadística e Informática (INEI) es el Órgano Rector de los Sistemas Nacionales de Estadística e Informática en el Perú." />        

        <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
        <link href="{{asset('assets/css/bootstrap.min.css?v=1.0')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/app.min.css?v=1.0')}}" rel="stylesheet" type="text/css" />
    </head>    
    <body>

        <div class="min-vh-100" style="background: url('{{ asset('assets/images/bg-2.png') }}') top;">
            <div class="bg-overlay bg-light"></div>
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-12">
                        <div class="text-center py-5 mt-5">                        
                            <a href="#" class="logo logo-dark">
                                <img src="{{asset('assets/images/logo-inei.png')}}" alt="Logo INEI" >
                            </a>
                        <div class="position-relative ">                            
                            <h1 class="error-title error-text mb-0">404</h1>
                            <h4 class="error-subtitle text-uppercase mb-0">¡PÁGINA NO ENCONTRADA!</h4>
                            <p class="font-size-16 mx-auto text-muted w-50 mt-4">Es posible que haya escrito mal la dirección o que la página se haya movido.</p>
                        </div>
                            <div class="mt-4 text-center">
                                <a class="btn btn-primary" href="{{route('asignaciones/conglomerado')}}">Regresar al Menu Principal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{asset('assets/js/bootstrap.bundle.min.js?v=1.0')}}"></script>
        <script src="{{asset('assets/js/metismenujs.min.js?v=1.0')}}"></script>
    </body>

</html>