<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MarcoEncuesta extends Model
{
    use HasFactory;

    protected $table = 'T_05_DIG_MARCO_ENCUESTA';
    protected $primaryKey = 'ID';
    public $incrementing= true;   
    protected $fillable = ['ID','CODSEDE', 'SEDE', 'UBIGEO', 'CCDD', 'CCPP', 'CCDI', 'CODCCPP',
        'DEPARTAMENTO', 'PROVINCIA', 'DISTRITO', 'NOMCCPP', 'AREA', 'CONGLOMERADOFINAL','ZONA',
        'AERINI_2018','AERFIN_2018', 'MZ_CCPP_1', 'TOTALMZ_CCPP', 'VIVPART2017', 'TIPOENAMEL', 
        'ESTRATO','ENTREVISTADOR','SUPERVISOR','COORDINADOR', 'PERIODO', 'FLAG'
    ]; 
    public $timestamps = false;
}
																								
