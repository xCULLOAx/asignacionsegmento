@extends('master.master')
@section('esTitulo')
    <title>Actualización de Segmentos | {{ config('app.name') }}</title>
@endsection 

@section('esBodyPrincipal')
    {{-- Enviamos los datos de usuarios al componente Vue, esto lo tomara como Props: lstAllFuncionarios --}}
    <actualizar-segmentos :lista-funcionarios="{{ $data }}"></actualizar-segmentos>
@endsection