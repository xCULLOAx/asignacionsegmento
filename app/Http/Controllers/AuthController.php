<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    // LOGIN
    public function irIngreso(Request $request){
        $usuario = Usuario::where('usuario', $request->email)->where('clave', $request->password)->first();  
        if ($usuario) {
            if ($usuario->HASHPASSWORD === NULL) {
               $usuario->HASHPASSWORD = bcrypt($request->password);
               $usuario->save();
            }
            //Personalizado
            if (Hash::check($request->password, $usuario->HASHPASSWORD)) {
                Auth::login($usuario, $request->filled('rememberme'));
                $request->session()->regenerate();
                return response()->json($this->getRolRutas());
                // return response()->json('asignaciones/conglomerado');
            }

            //Proceso correcto para Login con Email/Password
            /*
            $credencial = $request->only('email','password');
            $remember = $request->filled('remember');
            if (Auth::attempt($credencial, $remember)) {
                $request->session()->regenerate(); 
                return response()->json('principal');
            }*/
        }
    }  

    //LOGOUT
    public function irCerrarSesion(Request $request) { 
        Auth::logout(); 
        $request->session()->flush();
        $request->session()->invalidate();    
        $request->session()->regenerateToken();    
        return redirect()->intended('default');
    }

    public function getRolRutas(){
        $userRole = Auth::user()->getOnlyCargo();
        $rutasRoles = [
            'COORDINADOR' => [ 'asignaciones/conglomerado'],
            'ENCUESTADOR' => [ 'asignaciones/ruta' ],            
            'MONITOR' => ['segmentos/segmento' ],
            'MASTER' => [ 'asignaciones/conglomerado']
        ];        

        if (array_key_exists($userRole,$rutasRoles)) {
            $primerRutaPermitida = reset($rutasRoles[$userRole]);  
            return $rutasRoles[$userRole];  
        }
    }

}
