@extends('master.master')
@section('esTitulo')
    <title>Reporte de Marco Encuesta | {{ config('app.name') }}</title>
@endsection 

@section('esBodyPrincipal')
    <reporte-marco-encuesta></reporte-marco-encuesta>
@endsection
