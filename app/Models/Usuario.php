<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Usuario extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    
    protected $table = 'T_USUARIO';
    protected $primaryKey = 'ID';
    public $incrementing= true;   
    protected $fillable = ['ID','USUARIO', 'CLAVE', 'CARGO_ID', 'TELEFONO', 'EMAIL', 'ESTADO', 'NOMBRES', 
         'APELLIDOS', 'CORREO', 'DNI', 'COD_SEDE_OPERATIVA', 'EQUIPO', 'RUTA', 'HASHPASSWORD','REMEMBER_TOKEN'
    ]; 

    protected $username = 'USUARIO';
    protected $password = 'HASHPASSWORD';
    public $timestamps = false;
    protected $hidden = [
        'CLAVE', 'REMEMBER_TOKEN',
    ];

    public function hasRole(...$roles)
    {
        $cargoId = $this->attributes['CARGO_ID'];
        $userRole = 'MASTER';
        switch ($cargoId) {
            case 4:
                $userRole = 'COORDINADOR';
                break;
            case 18:
                $userRole = 'ENCUESTADOR';
                break;
            case 19:
                $userRole = 'MONITOR';
                break;
        }
        return in_array($userRole, $roles);
    }

    
    public function getCargonameAttribute()
    {
        $cargoId = $this->attributes['CARGO_ID'];
        switch ($cargoId) {
            case 4:
                return 'COORDINADOR DEPARTAMENTAL';
            case 18:
                return 'ENCUESTADOR';
            case 19:
                return 'MONITOR DE CAMPO';
            default:
                return 'MASTER';
        }
    }
  
    public function getOnlyCargo()
    {
        $cargoId = $this->attributes['CARGO_ID'];
        switch ($cargoId) {
            case 4:
                return 'COORDINADOR';
            case 18:
                return 'ENCUESTADOR';
            case 19:
                return 'MONITOR';
            default:
                return 'MASTER';
        }
    }

    public function getCargoRutaURL()
    {
        $cargoId = $this->attributes['CARGO_ID'];
        switch ($cargoId) {
            case 4:
                return 'asignaciones/conglomerado';
            case 18:
                return 'asignaciones/ruta';
            case 19:
                return 'segmentos/segmento';
            default:
                return 'asignaciones/conglomerado';
        }
    }
   
}
