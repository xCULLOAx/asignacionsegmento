<?php

use App\Http\Controllers\Asignaciones\AsignacionesController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UtilitariosController;
use App\Models\Usuario;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth' ], function() {
    Route::get('/', function () { return redirect(getHomeRouteForUser()); });

    /* ========================== ASIGNACIONES ==========================*/
    Route::view('/asignaciones/conglomerado',     'pages/asignaciones/AsigConglomeradoRuta')->name('asignaciones/conglomerado')->middleware('role:COORDINADOR,MASTER');
    Route::post('/getAsigConglomeradoRuta',       [AsignacionesController::class, 'ListarReportesAsignacion'])->name('getAsigConglomeradoRuta');
    Route::post('/SaveAsigConglomeradoRuta',      [AsignacionesController::class, 'GuardarAsigConglomeradoRuta'])->name('SaveAsigConglomeradoRuta');
    Route::post('/deleteAsigCongloRuta',          [AsignacionesController::class, 'EliminarAsignacionCongloRuta'])->name('deleteAsigCongloRuta');

    

    Route::view('/asignaciones/ruta', 'pages/asignaciones/AsigRutaNombre', [
        'data' => json_encode(Usuario::all())        
    ])->name('asignaciones/ruta')->middleware('role:ENCUESTADOR,MASTER'); 

    Route::post('/getAsigRutaNombre',       [AsignacionesController::class, 'ListarReportesAsignacion'])->name('getAsigRutaNombre');
    Route::post('/SaveAsigRutaNombre',      [AsignacionesController::class, 'GuardarAsigRutaNombre'])->name('SaveAsigRutaNombre');

    
    /* ========================== SEGMENTOS ==========================*/
    Route::view('/segmentos/segmento', 'pages/segmentos/ActualizaSegmentos', [
        'data' => json_encode(Usuario::all())        
    ])->name('segmentos/segmento')->middleware('role:MONITOR,MASTER'); 

    Route::post('/getActualizaSegmentos',       [AsignacionesController::class, 'ListarReportesSegmentos'])->name('getActualizaSegmentos');
    Route::post('/SaveActualizaSegmentos',      [AsignacionesController::class, 'GuardarActSegmentos'])->name('SaveActualizaSegmentos');
    Route::post('/deleteAsignacionSegmento',    [AsignacionesController::class, 'EliminarAsignacionSegmentos'])->name('deleteAsignacionSegmento');


    /* ========================== REPORTES ==========================*/
    Route::view('/reportes/asignaciones',   'pages/reportes/Asignaciones')->name('reportes/asignaciones')->middleware('role:MASTER');
    Route::view('/reportes/revertidos',     'pages/reportes/Revertidos')->name('reportes/revertidos')->middleware('role:MASTER');
    Route::post('/getSegmentosRevertidos',      [AsignacionesController::class, 'ListarReportesRevertidos'])->name('getSegmentosRevertidos');
    Route::view('/reportes/marco',     'pages/reportes/MarcoEncuesta')->name('reportes/marco')->middleware('role:MASTER');
    Route::post('/getMarcoEncuesta',      [AsignacionesController::class, 'ListarReportesMarcoEncuesta'])->name('getMarcoEncuesta');


    /* ========================== CARGAR EXCEL ==========================*/
    Route::view('/procesar/marco',   'pages/procesar/CargarMarco')->name('procesar/marco')->middleware('role:MASTER');
    Route::post('/procesar',         [AsignacionesController::class, 'ImportarExcelMarco'])->middleware('role:MASTER');

    /* ========================== FILTROS ==========================*/
    Route::get('/getAnios',             [UtilitariosController::class, 'listarAnios'])->name('getAnios');
    Route::post('/getMeses',            [UtilitariosController::class, 'listarMeses'])->name('getMeses');
    Route::post('/getSedeOpe',          [UtilitariosController::class, 'listarSedesOperativas'])->name('getSedeOpe');   
    Route::post('/getPeriodo',          [UtilitariosController::class, 'listarPeriodos'])->name('getPeriodo');
    Route::post('/getConglomerado',     [UtilitariosController::class, 'listarConglomerados'])->name('getConglomerado');
    Route::post('/getEquipos',          [UtilitariosController::class, 'listarEquipos'])->name('getEquipos');
    Route::post('/getRutas',            [UtilitariosController::class, 'listarRutas'])->name('getRutas');
    Route::post('/getUsuarios',         [UtilitariosController::class, 'listarUsuarios'])->name('getUsuarios');
});

/* ========================== AUTENTICACIÓN ==========================*/

Route::view('default',  'auth/login')->name('default')->middleware('guest');
Route::post('/login',   [AuthController::class, 'irIngreso']);
Route::get('/salir',    [AuthController::class, 'irCerrarSesion'])->name('salir');
Route::view('restriccion',  'restriccion')->name('restriccion');

function getHomeRouteForUser()
{
    if (Auth::check()) {
        if (Auth::user()->hasRole('ENCUESTADOR')) {
            return route('asignaciones/ruta');
        } elseif (Auth::user()->hasRole('MONITOR')) {
            return route('segmentos/segmento');
        }
    }

    return route('asignaciones/conglomerado'); 
}