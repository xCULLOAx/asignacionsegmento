<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta content="text/html; charset="utf-8" http-equiv="content-type"/>
        {{-- <title>INEI | Sistema de Asignación de Rutas</title> --}}
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="keywords" name="PERÚ,  INEI ,  Perú, economy , INFORMATICA, INSTITUTO, ECONOMIA, Statistical Instituto Nacional de Estadística e Informática,estadisticas, National Institute of Statistics, Censos, EncuestasIndicadores, PBI, pbi, IPC, ipc, Índice de Precios, indice de Precios, price tax" />
        <meta content="description" name="INEI PERÚ El Instituto Nacional de Estadística e Informática (INEI) es el Órgano Rector de los Sistemas Nacionales de Estadística e Informática en el Perú." />        

        @yield('esTitulo')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{asset('assets/images/favicon.ico')}}" rel="shortcut icon">
        <link href="{{asset('assets/DataTables-1.10.15/media/css/jquery.dataTables.css?v=1.0') }}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/bootstrap.min.css?v=1.0')}}" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.min.css?v=1.0')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/app.min.css?v=1.0')}}" id="app-style" rel="stylesheet" type="text/css" />
        @yield('esStyles') 
        <script type="text/javascript">
            window.APP_URL = '{{ asset("/") }}';
            window.APP_IMG = APP_URL+'assets/images/';      
        </script>
    </head>    
    <body>
        <div id="layout-wrapper">
            @include('master.header')
           
            @include('master.menu')  

            <div class="main-content">
                <div class="page-content">
                    <div class="container-fluid">
                        
                        <div id="appAsignacionSIS">
                            @yield('esBodyPrincipal') 
                        </div>                      

                    </div>
                </div>
                @include('master.footer')  
            </div>
        </div>       

        <!-- JAVASCRIPT -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js?v=1.0"></script>
        <script src="{{asset('assets/DataTables-1.10.15/media/js/jquery.dataTables.min.js?v=1.0') }}"></script> 
        <script src="{{asset('assets/js/bootstrap.bundle.min.js?v=1.0')}}"></script>
        <script src="{{asset('assets/js/metismenujs.min.js?v=1.0')}}"></script>
        <script src="{{asset('assets/js/simplebar.min.js?v=1.0')}}"></script>
        <script src="{{asset('assets/js/eva.min.js?v=1.0')}}"></script> 
        <script src="{{asset('assets/js/app.js?v=1.0')}}"></script>
        
        @vite('resources/js/app.js')
        <script type="text/javascript">            
            $(document).ready(function () {
                $("#IdInicio").removeClass("mm-active");
                $("#IdInicioURL").removeClass("active");
            });
        </script>
        @yield('esScript')
    </body>

</html>