<?php

namespace App\Imports\ExcelMarco;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

class PrincipalMarcoSegImport implements WithMultipleSheets, SkipsUnknownSheets
{
    use Importable;
    private $totalRows = 0;
    private $insertedRows = 0;
    private $existentRows = 0;
    
    public function getTotalRowCount()
    {
        return $this->totalRows;
    }

    public function getInsertedRowCount()
    {
        return $this->insertedRows;
    }

    public function getExistentRowCount()
    {
        return $this->existentRows;
    }

    public function sheets(): array
    {   
        return [
            'MARCO'       => new MarcoSegImport($this->totalRows, $this->insertedRows, $this->existentRows),
            // 'REG_COMUNICACION'      => new GRegionalesImport(),
        ];
    }

    public function onUnknownSheet($sheetName)
    {
        // E.g. you can log that a sheet was not found.
        info("Se omitió la hoja {$sheetName} ");
    } 
}

