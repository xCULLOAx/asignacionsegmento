<?php

namespace App\Imports\ExcelMarco;

use App\Models\MarcoEncuesta;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpsertColumns;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Throwable; 

class MarcoSegImport implements 
    ToModel,
    WithHeadingRow,
    WithBatchInserts,
    WithChunkReading,
    SkipsOnError,
    WithUpserts,
    WithUpsertColumns
{
    use Importable, SkipsErrors;

    private $totalRows;     //Filas procesadas
    private $insertedRows;  // Filas insertadas
    private $existentRows;  // Filas repetidas/existentes

    public function __construct(&$totalRows, &$insertedRows, &$existentRows)
    {
        $this->totalRows = &$totalRows;
        $this->insertedRows = &$insertedRows;
        $this->existentRows = &$existentRows;
    }

    private $rows = 0;

    public function uniqueBy()
    {
        return 'conglomeradofinal';
    }

    public function upsert()
    {
        return 'conglomeradofinal';
    }

    public function upsertColumns()
    {
        return ['codsede','ubigeo','ccdd','ccpp','ccdi','codccpp','conglomeradofinal'];
    }

    public function model(array $row)
    {
        ++$this->rows;
        ++$this->totalRows;
        
        // $row = array_change_key_case($row, CASE_UPPER); //Transformalos en Mayusculas

        $dataArray = [
            'CODSEDE'       => $row['codsede'],
            'SEDE'          => $row['sede'],
            'UBIGEO'        => $row['ubigeo'],           
            'CCDD'          => $row['ccdd'], 
            'CCPP'          => $row['ccpp'],
            'CCDI'          => $row['ccdi'],
            'CODCCPP'       => $row['codccpp'],
            'DEPARTAMENTO'   => mb_strtoupper($row['departamento'],'utf-8'),
            'PROVINCIA'      => mb_strtoupper($row['provincia'],'utf-8'),
            'DISTRITO'       => mb_strtoupper($row['distrito'],'utf-8'),
            'NOMCCPP'        => mb_strtoupper($row['nomccpp'],'utf-8'),
            'AREA'           => $row['area'],
            'CONGLOMERADOFINAL'            => $row['conglomeradofinal'],		
            'ZONA'            => $row['zona'],
            'AERINI_2018'     => $row['aerini_2018'], 
            'AERFIN_2018'     => $row['aerfin_2018'],
            'MZ_CCPP_1'       => $row['mz_ccpp_1'],
            'TOTALMZ_CCPP'    => $row['totalmz_ccpp'], 
            'VIVPART2017'     => $row['vivpart2017'], 
            'TIPOENAMEL'      => $row['tipoenamel'], 
            'ESTRATO'         => $row['estrato'], 
            'ENTREVISTADOR'   => $row['entrevistador'], 
            'SUPERVISOR'      => $row['supervisor'], 
            'COORDINADOR'     => $row['coordinador'], 
            'PERIODO'         => $row['periodo'], 
        ];
        $exists = MarcoEncuesta::where('CODSEDE', $row['codsede'])
                ->where('UBIGEO', $row['ubigeo'])            
                ->where('CCDD', $row['ccdd'])
                ->where('CCPP', $row['ccpp'])
                ->where('CCDI', $row['ccdi'])
                ->where('CODCCPP', $row['codccpp'])
                ->where('CONGLOMERADOFINAL', $row['conglomeradofinal'])->first(); 
        if ($exists) {
            ++$this->existentRows;
            $exists->update($dataArray);
            return null;
        }else{  
            ++$this->insertedRows;
            MarcoEncuesta::create($dataArray);
        }
        return null;
    }



    public function onError(Throwable $error)
    {
        return $error;
    }

    public function getRowCount(): int
    {
        return $this->rows;
    }

    public function batchSize(): int
    {
        return 5000;
    }

    public function chunkSize(): int
    {
        return 5000;
    }
}