<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */

    public function handle(Request $request, Closure $next, ...$roles)
    {
        $rutasRoles = [
            'COORDINADOR' => [ 'asignaciones/conglomerado'],
            'ENCUESTADOR' => [ 'asignaciones/ruta' ],            
            'MONITOR' => ['segmentos/segmento' ],
        ]; 
  
        $rutasConTitulos = [
            'COORDINADOR' => 'ASIGNACIÓN DE CONGLOMERADO A RUTA',
            'ENCUESTADOR' => 'ASIGNACIÓN DE RUTA A NOMBRE',
            'MONITOR'     => 'ACTUALIZAR SEGMENTO'
        ];    
        $actualRutaNombre = Route::currentRouteName();       
        $tituloRuta = $rutasConTitulos[$request->user()->getOnlyCargo()] ?? '';
        

        if (!$request->user() || !$request->user()->hasRole(...$roles)) {
            // abort(403, 'No tienes permisos para acceder a esta página.');
            $roleActual = $request->user()->getCargonameAttribute();
            if (!in_array($actualRutaNombre, $rutasRoles[$roleActual])) {
                $primerRutaPermitida = reset($rutasRoles[$roleActual]);               
                session()->put('rutaRolPermitida', $primerRutaPermitida);
                session()->put('rutaTituloRolPermitida', $tituloRuta);
                return redirect('restriccion');
            }
        }
        session()->forget('rutaRolPermitida');
        session()->forget('rutaTituloRolPermitida');

        return $next($request);
    }
}
