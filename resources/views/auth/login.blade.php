@extends('auth.master')
@section('esTitulo')
    <title>Inicio Sesión | {{ config('app.name') }}</title>
@endsection 

@section('esBodyAuth')
    {{-- Cargar el componente Vue a mostrar  --}}
	<login-auth></login-auth>
@endsection 
