<?php

namespace App\Http\Controllers\Asignaciones;

use App\Http\Controllers\Controller;
use App\Imports\ExcelMarco\MarcoSegImport;
use App\Imports\ExcelMarco\PrincipalMarcoSegImport;
use App\Models\AsignacionCongloRuta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class AsignacionesController extends Controller
{
    public function ListarReportesAsignacion(Request $request) {
        $data = $this->ejecutarSPReportes($request);
        return response()->json($data);
    }
    
    public function GuardarAsigConglomeradoRuta(Request $request) {
        foreach ($request->listChecks as $conglomerado) {
            AsignacionCongloRuta::create([
                'ANIO' => $request->anio,
                'MES' => $request->mes,
                'COD_SEDE' => $request->codsede,
                'PERIODO' => $request->periodo,
                'USUARIO_ID' => Auth::user()->ID,
                'USUARIO' => Auth::user()->USUARIO,
                'FECHA_ASIGCONGLO' => Carbon::now(),
                'ESTADO' => 1,
                'EQUIPO' => $request->equipo,
                'RUTA' => $request->ruta,
                'CONGLOMERADO' => $conglomerado
            ]);
        }
        
        $request->merge([
            'tipo' => 'REPORTEASIG_CONGLORUTA',
            'conglomerado' => $request->conglo
        ]);

        $data = $this->ejecutarSPReportes($request);

        return response()->json([
            'message' => 'Asignación exitosa',
            'lista' => $data
        ], 200);
    }

    public function EliminarAsignacionCongloRuta(Request $request) {
        AsignacionCongloRuta::find($request->id)->delete();
        $request->merge(['tipo' => 'REPORTEASIG_CONGLORUTA']);
        $data = $this->ejecutarSPReportes($request);

        return response()->json([
            'message' => 'Asignación exitosa',
            'lista' => $data
        ], 200);
    }

    public function GuardarAsigRutaNombre(Request $request) {
        $data = AsignacionCongloRuta::find($request->id);
        $data->update([
            'DNI'       => $request->dni, 
            'NOMBRES'   => $request->nombres,
            'FECHA_ASIGNOMBRE'  => Carbon::now()
        ]);
        $request->merge(['tipo' => 'REPORTEASIG_RUTANOMBRE']);
        $data = $this->ejecutarSPReportes($request);
        return response()->json([
            'message' => 'Asignación exitosa',
            'lista' => $data
        ], 200);
    }




    public function ListarReportesSegmentos(Request $request) {
        // $data = DB::select('EXEC SP_REPORTES_ASIGNACIONES_SEGMENTOS ?,?,?,?,?,?,?',
        // array($request->tipo,$request->anio,$request->codsede,$request->mes,$request->periodo,$request->conglomerado,Auth::user()->USUARIO));
        $data = $this->ejecutarSPReportes($request);
        return response()->json($data);
    }
    
    public function GuardarActSegmentos(Request $request) {
        $data = AsignacionCongloRuta::find($request->id);
        $data->update([
            'EQUIPO'    => $request->equipo,
            'RUTA'      => $request->ruta,
            'DNI'       => $request->dni, 
            'NOMBRES'   => $request->nombres,
            'FECHA_SEGMENTO'  => Carbon::now()
        ]);

        $request->merge(['tipo' => 'REPORTEACT_SEGMENTO']);
        $data = $this->ejecutarSPReportes($request);

        return response()->json([
            'message' => 'Asignación exitosa',
            'lista' => $data
        ], 200);

    }
    
    public function ListarReportesRevertidos(Request $request) {
        $data = $this->ejecutarSPReportes($request);
        return response()->json($data);
    }  

    public function ListarReportesMarcoEncuesta(Request $request) {
        $request->merge([
            'anio'  => '9999',
            'mes'   => '99',
            'periodo' => '9',
            'conglomerado' => '99'
        ]);
        $data = $this->ejecutarSPReportes($request);
        return response()->json($data);
    }

    public function EliminarAsignacionSegmentos(Request $request) {
        // AsignacionCongloRuta::find($request->id)->delete();
        $data = AsignacionCongloRuta::find($request->id);
        $data->update([
            'ESTADO'  => 2,
            'FECHA_REVERTIDO'  => Carbon::now()
        ]);

        $request->merge(['tipo' => 'REPORTEACT_SEGMENTO']);
        $data = $this->ejecutarSPReportes($request);

        return response()->json([
            'message' => 'Asignación exitosa',
            'lista' => $data
        ], 200);
    }


    public function ImportarExcelMarco(Request $request){
        $file = $request->file('filemarco');
        $import = new PrincipalMarcoSegImport();
        Excel::import($import, $file);
        $totalRows = $import->getTotalRowCount();
        $insertedRows = $import->getInsertedRowCount();
        $existentRows = $import->getExistentRowCount();

        return response()->json([
            'message' => 'Operación exitosa',
            'totalRows' => $totalRows, // Obtener el recuento total de filas procesadas
            'insertedRows' => $insertedRows, // Obtener el recuento de filas insertadas
            'existentRows' => $existentRows, // Obtener el recuento de filas existentes/duplicadas
        ]);
    }

    private function ejecutarSPReportes(Request $request)
    {
        return DB::select('EXEC SP_REPORTES_ASIGNACIONES_SEGMENTOS ?,?,?,?,?,?,?',
            array(
                $request->tipo,
                $request->anio,
                $request->codsede,
                $request->mes,
                $request->periodo,
                $request->conglomerado,
                Auth::user()->USUARIO
            )
        );
    }
}
