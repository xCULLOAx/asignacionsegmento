@extends('master.master')
@section('esTitulo')
    <title>Asig. Conglomerado a Rutas | {{ config('app.name') }}</title>
@endsection 

@section('esBodyPrincipal')
    <asignacion-conglomerado-rutas></asignacion-conglomerado-rutas>
@endsection
