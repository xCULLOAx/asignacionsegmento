@extends('master.master')
@section('esTitulo')
    <title>Reporte de Asignaciones | {{ config('app.name') }}</title>
@endsection 

@section('esBodyPrincipal')
    <reporte-asignaciones></reporte-asignaciones>
@endsection
