@extends('master.master')
@section('esTitulo')
    <title>Reporte de Asignaciones Revertidas | {{ config('app.name') }}</title>
@endsection 

@section('esBodyPrincipal')
    <reporte-revertidos></reporte-revertidos>
@endsection
