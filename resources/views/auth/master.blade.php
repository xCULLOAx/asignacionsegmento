<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>

        <meta content="text/html; charset="utf-8" http-equiv="content-type"/>
        {{-- <title>INEI | Sistema de Asignación de Rutas</title> --}}
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="keywords" name="PERÚ,  INEI ,  Perú, economy , INFORMATICA, INSTITUTO, ECONOMIA, Statistical Instituto Nacional de Estadística e Informática,estadisticas, National Institute of Statistics, Censos, EncuestasIndicadores, PBI, pbi, IPC, ipc, Índice de Precios, indice de Precios, price tax" />
        <meta content="description" name="INEI PERÚ El Instituto Nacional de Estadística e Informática (INEI) es el Órgano Rector de los Sistemas Nacionales de Estadística e Informática en el Perú." />        

        <title>Login | Sistema de Asignación de Rutas</title>
        @yield('esTitulo')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
        <link href="{{asset('assets/css/bootstrap.min.css?v=1.0')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.min.css?v=1.0')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/app.min.css?v=1.0')}}" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            window.APP_URL = '{{ asset("/") }}';
            window.APP_IMG = APP_URL+'assets/images/';      
        </script>
    </head>
    
    <body>
        <div id="appAsignacionSIS">
            @yield('esBodyAuth') 
        </div>
        <!-- JAVASCRIPT -->
        <script src="{{asset('assets/js/bootstrap.bundle.min.js?v=1.0')}}"></script>
        <script src="{{asset('assets/js/metismenujs.min.js?v=1.0')}}"></script>
        <script src="{{asset('assets/js/simplebar.min.js?v=1.0')}}"></script>
        <script src="{{asset('assets/js/eva.min.js?v=1.0')}}"></script>
        {{-- <script src="{{asset('assets/js/pass-addon.init.js')}}"></script> --}}

        @vite('resources/js/app.js')
        @yield('esScript')
    </body>
</html>