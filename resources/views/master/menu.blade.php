<div class="vertical-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box text-center">
        <a href="#" class="logo logo-dark">
            <span class="logo-sm">
                <img src="{{asset('assets/images/logo-inei.png')}}" alt="" height="26">
            </span>
            <span class="logo-lg">
                <img src="{{asset('assets/images/logo-inei.png')}}" alt="" height="65">
            </span>
        </a>        
        {{-- <a href="index.html" class="logo logo-light">
            <span class="logo-lg">
                <img src="{{asset('assets/images/logo-light.png')}}" alt="" height="30">
            </span>
            <span class="logo-sm">
                <img src="{{asset('assets/images/logo-light-sm.png')}}" alt="" height="26">
            </span>
        </a> --}}
    </div>

    <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect vertical-menu-btn">
        <i class="bx bx-menu align-middle"></i>
    </button>

    <div data-simplebar class="sidebar-menu-scroll">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled mt-4" id="side-menu">

                <li class="menu-title" data-key="t-layouts">Principal</li>                
                <li id="IdInicio">
                    <a href="{{ route(Auth::user()->getCargoRutaURL())}}" id="IdInicioURL">
                        <i class="bx bx-home-alt icon nav-icon"></i>
                        <span class="menu-item" data-key="t-horizontal">Inicio</span>
                    </a>
                </li>

                <li class="menu-title" data-key="t-menu">Asignaciones</li>
                <li id="IdAsignaciones">
                    <a href="javascript: void(0);" class="has-arrow">
                        <i class="bx bx-pie-chart-alt-2 icon nav-icon"></i>
                        <span class="menu-item" >Asignación de Carga</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('asignaciones/conglomerado')}}">Asignación de <br>Conglomerado a<br> Ruta</a></li>
                        <li><a href="{{route('asignaciones/ruta')}}">Asignación de <br>Ruta a Nombre</a></li>
                    </ul>
                </li>
                <li class="menu-title" data-key="t-menu">Segmentos</li>
                <li id="IdSegmentos">
                    <a href="{{route('segmentos/segmento')}}">
                        <i class="bx bx-cube icon nav-icon"></i>
                        <span class="menu-item">Actualizar Segmento</span>
                    </a>
                </li>
                <li class="menu-title" data-key="t-menu">Reportes Generales</li>
                <li id="IdReportes">
                    <a href="javascript: void(0);" class="has-arrow">
                        <i class="bx bx-file icon nav-icon"></i>
                        <span class="menu-item" >Reportes</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('reportes/asignaciones')}}" >Reporte de <br>Asignaciones</a></li>
                        <li><a href="{{route('reportes/revertidos')}}">Reporte de <br>Asignaciones <br>Revertidas</a></li>
                        <li><a href="{{route('reportes/marco')}}">Reporte de <br>Marco <br>Encuesta</a></li>
                    </ul>
                </li>
                <li class="menu-title" data-key="t-menu">Procesar Excel</li>
                <li id="IdSegmentos">
                    <a href="{{route('procesar/marco')}}">
                        <i class="bx bx-cloud-upload icon nav-icon"></i>
                        <span class="menu-item">Cargar Marco</span>
                    </a>
                </li>
                <li class="menu-title" data-key="t-pages">Cerrar Sesión</li>

                <li>
                    <a href="{{route('salir')}}">
                        <i class="bx bx-log-in icon nav-icon"></i>
                        <span class="menu-item" data-key="t-authentication">Salir</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
