@extends('master.master')
@section('esTitulo')
    <title>Asig. Rutas a Nombre | {{ config('app.name') }}</title>
@endsection 

@section('esBodyPrincipal')
    {{-- Enviamos los datos de usuarios al componente Vue, esto lo tomara como Props: lstAllFuncionarios --}}
    <asignacion-ruta-nombres :lista-funcionarios="{{ $data }}"></asignacion-ruta-nombres>
@endsection