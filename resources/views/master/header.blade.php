<header id="page-topbar" class="isvertical-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box text-center">
                <a href="#" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{asset('assets/images/logo-inei.png')}}" alt="" height="26">
                    </span>
                    <span class="logo-lg">
                        <img src="{{asset('assets/images/logo-inei.png')}}" alt="" height="65">
                    </span>
                </a>
                {{-- <a href="index.html" class="logo logo-light">
                    <span class="logo-lg">
                        <img src="{{asset('assets/images/logo_inei.png')}}" alt="" height="30">
                    </span>
                    <span class="logo-sm">
                        <img src="{{asset('assets/images/logo-light-sm.png')}}" alt="" height="26">
                    </span>
                </a> --}}
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect vertical-menu-btn">
                <i class="bx bx-menu align-middle"></i>
            </button>
        </div>
        <div class="d-flex">
            <!-- start page title -->
            <div class="page-title-box align-self-center d-none d-md-block text-center">
                <h6 class="page-title fw-semibold display-4 mb-0">SISTEMA DE ASIGNACIÓN DE RUTAS Y ACTUALIZACIÓN DE SEGMENTACIÓN 2023</h6>
            </div>
            <!-- end page title -->

        </div>

        <div class="d-flex">

            {{-- <div class="dropdown d-inline-block language-switch ms-2">
                <button type="button" class="btn header-item" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="header-lang-img" src="assets/images/us.jpg" alt="Header Language" height="18">
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item language" data-lang="sp">
                        <img src="assets/images/spain.jpg" alt="user-image" class="me-1" height="12"> <span class="align-middle">Spanish</span>
                    </a>
                </div>
            </div>

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item noti-icon"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="bx bx-search icon-sm align-middle"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0">
                    <form class="p-2">
                        <div class="search-box">
                            <div class="position-relative">
                                <input type="text" class="form-control rounded bg-light border-0" placeholder="Search...">
                                <i class="bx bx-search search-icon"></i>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item noti-icon" id="page-header-notifications-dropdown-v"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="bx bx-bell icon-sm align-middle"></i>
                    <span class="noti-dot bg-danger rounded-pill">4</span>
                </button>
                <div class="dropdown-menu dropdown-menu-xl dropdown-menu-end p-0"
                    aria-labelledby="page-header-notifications-dropdown-v">
                    <div class="p-3">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5 class="m-0 font-size-15"> Notifications </h5>
                            </div>
                            <div class="col-auto">
                                <a href="#!" class="small fw-semibold text-decoration-underline"> Mark all as read</a>
                            </div>
                        </div>
                    </div>
                    <div data-simplebar style="max-height: 250px;">
                        <a href="#!" class="text-reset notification-item">
                            <div class="d-flex">
                                <div class="flex-shrink-0 me-3">
                                    <img src="assets/images/avatar-3.jpg" class="rounded-circle avatar-sm" alt="user-pic">
                                </div>
                                <div class="flex-grow-1">
                                    <p class="text-muted font-size-13 mb-0 float-end">1 hour ago</p>
                                    <h6 class="mb-1">James Lemire</h6>
                                    <div>
                                        <p class="mb-0">It will seem like simplified English.</p>
                                    </div>
                                </div>
                    
                            </div>
                        </a>
                        <a href="#!" class="text-reset notification-item">
                            <div class="d-flex">
                                <div class="flex-shrink-0 avatar-sm me-3">
                                    <span class="avatar-title bg-primary rounded-circle font-size-18">
                                        <i class="bx bx-cart"></i>
                                    </span>
                                </div>
                                <div class="flex-grow-1">
                                    <p class="text-muted font-size-13 mb-0 float-end">3 min ago</p>
                                    <h6 class="mb-1">Your order is placed</h6>
                                    <div>
                                        <p class="mb-0">If several languages coalesce the grammar</p>
                                    </div>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="p-2 border-top d-grid">
                        <a class="btn btn-sm btn-link font-size-14 btn-block text-center" href="javascript:void(0)">
                            <i class="uil-arrow-circle-right me-1"></i> <span>View More..</span>
                        </a>
                    </div>
                </div>
            </div> --}}

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item user text-start d-flex align-items-center"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user" src="{{asset('assets/images/anonimo.png')}}" alt="Avatar">
                    <span class="d-none d-xl-inline-block ms-2 fw-medium font-size-15">
                        @if(Auth::check())
                            {{Auth::user()->USUARIO}}
                        @else 
                            INEI
                        @endif
                    </span>
                </button>
                @if(Auth::check())
                    <div class="dropdown-menu dropdown-menu-end pt-0">
                        <div class="p-3 border-bottom text-center">
                            <h6 class="mb-0">{{Auth::user()->cargoname }}</h6>
                            <p class="mb-0 font-size-11 "> {{Auth::user()->NOMBRES }}</p>
                        </div>
                        {{-- <div class="dropdown-divider"></div> --}}
                        <a class="dropdown-item" href="{{route('salir')}}">
                            <i class="mdi mdi-logout text-muted font-size-16 align-middle me-2"></i> 
                            <span class="align-middle">Cerrar Sesión</span>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</header>