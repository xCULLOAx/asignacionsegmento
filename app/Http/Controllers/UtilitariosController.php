<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UtilitariosController extends Controller
{
    
    public function listarAnios() {
        $data = DB::select('EXEC SP_FILTROS ?,?,?,?,?,?,?',array('ANIO',null,null,null,null,null,Auth::user()->USUARIO));
        return response()->json($data);
    }

    public function listarSedesOperativas(Request $request) { 
        $data = DB::select('EXEC SP_FILTROS ?,?,?,?,?,?,?',array($request->tipo,null,null,null,null,null,Auth::user()->USUARIO));
        return response()->json($data);
    }

    public function listarMeses(Request $request) { 
        $data = DB::select('EXEC SP_FILTROS ?,?,?,?,?,?,?',array($request->tipo,$request->anio,null,null,null,null,Auth::user()->USUARIO));
        return response()->json($data);
    }

    public function listarPeriodos(Request $request) {
        $data = DB::select('EXEC SP_FILTROS ?,?,?,?,?,?,?',array($request->tipo,$request->anio,$request->codsede,$request->mes,null,null,Auth::user()->USUARIO));
        return response()->json($data);
    }

    public function listarConglomerados(Request $request) {
        $data = DB::select('EXEC SP_FILTROS ?,?,?,?,?,?,?',array($request->tipo,$request->anio,$request->codsede,$request->mes,$request->periodo,null,Auth::user()->USUARIO));
        return response()->json($data);
    }
    

    public function listarEquipos(Request $request) {
        $data = DB::select('EXEC SP_FILTROS ?,?,?,?,?,?,?',array($request->tipo,$request->anio,$request->codsede,$request->mes,$request->periodo,null,Auth::user()->USUARIO));
        return response()->json($data);
    }
    
    public function listarRutas(Request $request) {
        $data = DB::select('EXEC SP_FILTROS ?,?,?,?,?,?,?',array($request->tipo,$request->anio,$request->codsede,$request->mes,$request->periodo,null,Auth::user()->USUARIO));
         return response()->json($data);
    }

}
