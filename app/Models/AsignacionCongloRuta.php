<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsignacionCongloRuta extends Model
{
    use HasFactory;

    protected $table = 'T_05_DIG_ASIGNACIONES_SEGMENTOS';
    protected $primaryKey = 'ID';
    public $incrementing= true;   
    protected $fillable = ['ID','ANIO', 'MES', 'COD_SEDE', 'PERIODO', 'CONGLOMERADO', 'EQUIPO', 'RUTA',
        'USUARIO_ID', 'USUARIO', 'DNI', 'NOMBRES', 'FECHA_ASIGCONGLO', 'FECHA_ASIGNOMBRE','FECHA_REVERTIDO','FECHA_SEGMENTO','ESTADO'
    ]; 
    public $timestamps = false;
}
