<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta content="text/html; charset="utf-8" http-equiv="content-type"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="keywords" name="PERÚ,  INEI ,  Perú, economy , INFORMATICA, INSTITUTO, ECONOMIA, Statistical Instituto Nacional de Estadística e Informática,estadisticas, National Institute of Statistics, Censos, EncuestasIndicadores, PBI, pbi, IPC, ipc, Índice de Precios, indice de Precios, price tax" />
        <meta content="description" name="INEI PERÚ El Instituto Nacional de Estadística e Informática (INEI) es el Órgano Rector de los Sistemas Nacionales de Estadística e Informática en el Perú." />        

        <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
        <link href="{{asset('assets/css/bootstrap.min.css?v=1.0')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.min.css?v=1.0')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/app.min.css?v=1.0')}}" rel="stylesheet" type="text/css" />
    </head>    
    <body>

        <div class="authentication-bg min-vh-100">
            <div class="bg-overlay bg-light"></div>
            <div class="container">
                <div class="d-flex flex-column min-vh-100 px-3 pt-4">
                    <div class="row justify-content-center my-auto">
                        <div class="col-md-8 col-lg-6 col-xl-5">

                            <div class="mb-4 pb-2 text-center">
                                <a href="index.html" class="d-block auth-logo">
                                    <img src="{{asset('assets/images/logo-inei.png')}}" alt="Logo INEI" >
                                </a>
                            </div>

                            <div class="card">
                                <div class="card-body p-4"> 
                                    <div class="p-2 my-2 text-center">
                                        <div class="avatar-lg mx-auto">
                                            <div class="avatar-title rounded-circle bg-light">
                                                <i class="mdi mdi-lock h2 mb-0 text-primary"></i>
                                            </div>
                                        </div>

                                        <div class="mt-4 pt-1">
                                            <h4>Acceso restringido !</h4>
                                            <p class="text-muted">No tienes permisos para acceder a este módulo. <br> Contácte al administrador para verificar sus permisos.</p>
                                            <div class="mt-4">
                                                @if (session('rutaRolPermitida'))
                                                    <a href="{{ route(session('rutaRolPermitida')) }}" class="btn btn-primary w-100">
                                                        Regresar a Módulo                                                        
                                                    </a>
                                                @endif                                                
                                            </div>
                                            <p class="mt-2"><strong>{{ session('rutaTituloRolPermitida') }}</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center p-4">
                                <p>© <script>document.write(new Date().getFullYear())</script> © INEI - DNCE INFORMÁTICA</p>
                            </div> 
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <script src="{{asset('assets/js/bootstrap.bundle.min.js?v=1.0')}}"></script>
        <script src="{{asset('assets/js/metismenujs.min.js?v=1.0')}}"></script>

    </body>

</html>