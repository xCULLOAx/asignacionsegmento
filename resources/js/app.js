import './bootstrap';
import { createApp } from 'vue'

import { useVuelidate } from '@vuelidate/core'
import * as validators from '@vuelidate/validators';
import Swal from 'sweetalert2';
import vSelect from "vue-select";
import "vue-select/dist/vue-select.css";

//Importando Componentes
import LoginAuth from './components/AuthVue/Login.vue';
//Modulo de Asignaciones
import AsigConglomeradoRuta from './components/Asignaciones/AsigConglomeradoRuta.vue';
import AsigRutaNombre from './components/Asignaciones/AsigRutaNombre.vue';
//Modulo de Segmentos
import ActualizaSegmentos from './components/Segmentos/ActualizarSegmentos.vue';
//Modulo de Reportes
import ReporteAsignaciones from './components/Reportes/ReporteAsignaciones.vue';
import ReporteRevertidos from './components/Reportes/ReporteRevertidos.vue';
import ReporteMarcoEncuesta from './components/Reportes/ReporteMarcoEncuesta.vue';
//Modulo de Carga de Excel
import CargarExcelMarco from './components/Procesar/CargarMarco.vue';

const appAsignacionSIS = createApp({});

//Cargando Librerias
appAsignacionSIS.use(useVuelidate);
appAsignacionSIS.use(vSelect);

//Asignando Alias a componente
appAsignacionSIS.component('login-auth', LoginAuth);
appAsignacionSIS.component('asignacion-conglomerado-rutas', AsigConglomeradoRuta);
appAsignacionSIS.component('asignacion-ruta-nombres', AsigRutaNombre);
appAsignacionSIS.component('actualizar-segmentos', ActualizaSegmentos);
appAsignacionSIS.component('reporte-asignaciones', ReporteAsignaciones);
appAsignacionSIS.component('reporte-revertidos', ReporteRevertidos);
appAsignacionSIS.component('reporte-marco-encuesta', ReporteMarcoEncuesta);
appAsignacionSIS.component('cargar-excel-marco', CargarExcelMarco);
//Montamos el Componente Principal en una Etiqueta que se llamara desde Blade.
appAsignacionSIS.mount('#appAsignacionSIS');



