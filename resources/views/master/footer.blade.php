<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script> © INEI.
            </div>
            <div class="col-sm-6">
                <div class="text-sm-end d-none d-sm-block">
                    DNCE INFORMÁTICA
                </div>
            </div>
        </div>
    </div>
</footer>